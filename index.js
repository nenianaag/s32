
let http = require("http")

let port = 4000

http.createServer((request, response) => {
	// Checks if the endpoint is equal to /items and the request.method is equal to GET
	if(request.url == "/" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data retrieved from the database!')
	}

	// Checks if the endpoint is equal to /items and the request.method is equal to POST
	if(request.url == "/items" && request.method == "POST") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data has been inserted into the database!')
	}
}).listen(port)

console.log(`Server is running at localhost:${port}`)